**Vous pouvez accéder directement à cette URL:**
https://nbviewer.jupyter.org/github/asmaa80/Etic_Projet/blob/main/Egl.ipynb


_**Organisation du projet**_

![Organisation du projet](/2_Schema.png)


_**Nous devons d'abord installer:**_

```
- sqlites pour créer la base de données
- python pour le codage
- metabase pour l'analyse et la visualisation
```
 
_**Étapes de programmation:**_

```
- Au premier nous avons 15 fichiers en TXT pour lyon avec des en-têtes: le nom de l'étage, Heure, Celsius (° C), Numéro de série

- deuxièmement, nous avons 10 fichiers en TXT et csv pour Grenoble avec des en-têtes: le nom de l'étage, - l'heure, Celsius (° C), le numéro de série 

- donc j'ai changé les en-têtes au début pour chaque fichier et j'ai mis le numéro de Numéro de série dans chaque cellule après cela j'ai mis chaque 5 fichiers ensemble (qui ont le même Numéro de série)

- nous avons donc 5 fichiers à Lyon et 2 fichiers à grenoble (qui représentent les tempretures en hiver et en été)
- j'ai donc combiné les 5 fichiers à Lyon dans un fichier nommé Etic_lyon.csv

- qui ont ces colonnes: ville, etage, dates, heure, celsius, série

- j'ai fait la même chose pour grenoble j'ai le fichier Etic_grenoble.csv 

- Pour l'API, j'ai utilisé le site Internet (meteomatics) pour prendre toute la météo historique, je télécharge l'API directement en tant que csv, j'avais 2 colonnes qui sont (validdate; t_2m: C) donc j'ai séparé les en-têtes et changé le nom du en-têtes à (date Celsius) après cela, j'ai séparé les dates et l'heure et j'ai ajouté 3 colonnes (ville, etage, série), j'ai maintenant l'API pour Lyon et Grenoble nommée Api_all.csv 

- J'ai donc fusionné les trois fichiers dans un seul fichier  appelé Etic_API.csv

- après que je crée une base de données en utilisant Sqlite3 et j'ai téléchargé sur la métabase pour la visualisation 


```
_**ETL**_
![ETL](/ETL.png)




**_Les ressources:_**


https://www.dataindependent.com/pandas/pandas-change-column-names/

https://www.geeksforgeeks.org/how-to-get-column-names-in-pandas-dataframe/

https://www.freecodecamp.org/news/how-to-combine-multiple-csv-files-with-8-lines-of-code-265183e0854/

https://stackoverflow.com/questions/36688022/removing-header-column-from-pandas-dataframe

https://towardsdatascience.com/transforming-variables-in-a-pandas-dataframe-bce2c6ef91a1

https://towardsdatascience.com/simple-data-visualisation-with-pandas-7a7a035bdc39

https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.show.html




